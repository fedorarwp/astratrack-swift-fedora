//
//  MainMenuViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 05/12/22.
//

import Alamofire
import UIKit
import Kingfisher

class MainMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TopUpViewControllerDelegate{

    @IBOutlet weak var tableMenu1: UITableView!
    
    var collectionPromo:UICollectionView!
    var collectionProduk:UICollectionView!
    
    var saldo: Double = 0.0
    var nama: String = ""
    var labelSaldo: UILabel!
    
    var arrayLinkPromosi = ["https://astrapay.com/static-assets/images/upload/promo/main/ppob17nov_903x510.png",
                     "https://astrapay.com/static-assets/images/upload/promo/main/dayaauto_903x510.png",
                     "https://astrapay.com/static-assets/images/upload/promo/main/daytrans_september_903x510.png",
                     "https://astrapay.com/static-assets/images/upload/promo/main/transjatim_sept_903x510.png",
                     "https://astrapay.com/static-assets/images/upload/promo/main/Trans%20jogja_sept2022_903x510_AP.jpg",
                     "https://astrapay.com/static-assets/images/upload/promo/main/trans_semarang_sept2022_903x510_AP.jpg",
                     "https://astrapay.com/static-assets/images/upload/promo/main/sayurbox_oct_903x510.png"]
    var arrayLinkProduk = [
            "https://maucash.id/wp-content/uploads/2022/10/Maumodal-app-banner-AP-03.jpg",
            "https://maucash.id/wp-content/uploads/2022/10/Astrapay-November-22-02-scaled.jpg",
            "https://astrapay.com/static-assets/images/upload/blog/main/2022/10/fraudawareness_onlineshop_blog903x903.png",
            "https://astrapay.com/static-assets/images/upload/blog/main/2022/07/externalfraud_juli_gantiPIN_1080x1080.png"
        ]
    var arrayLabelProduk = ["Maucash (Pinjaman Online)", "Maupaylater", "Maumodal", "Permatamoxaku"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCurrentUser()
        
        tableMenu1.delegate = self
        tableMenu1.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(methodOfReceivedNotification), name:Notification.Name("UpdateSaldoAPMainMenu"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(){
            getCurrentUser()
        }
    
    @objc func astraTrackButtonTapped() {
        let astraTrackMainViewController = self.storyboard?.instantiateViewController(withIdentifier: "AstraTrackMainViewController") as! AstraTrackMainViewController
        astraTrackMainViewController.saldo = self.saldo
        self.navigationController?.pushViewController(astraTrackMainViewController, animated: true)
    }
    
    @objc func topupButtonTapped() {
        let topupViewController = self.storyboard?.instantiateViewController(withIdentifier: "TopUpViewController") as! TopUpViewController
        topupViewController.delegate = self
        
        self.navigationController?.pushViewController(topupViewController, animated: true)
    }
    
    @objc func scanButtonTapped() {
        let bayarViewController = self.storyboard?.instantiateViewController(withIdentifier: "BayarViewController") as! BayarViewController
        
        bayarViewController.saldo = self.saldo
        
        self.navigationController?.pushViewController(bayarViewController, animated: true)
    }
    
    @objc func settingButtonTapped() {
        let settingViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController")
        
        self.navigationController?.pushViewController(settingViewController!, animated: true)
    }
    
    @objc func riwayatButtonTapped() {
        let riwayatViewController = self.storyboard?.instantiateViewController(withIdentifier: "RiwayatViewController")
        
        self.navigationController?.pushViewController(riwayatViewController!, animated: true)
    }
    
   func getCurrentUser() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        AF.request("http://localhost:8080/astratrack/currentuser", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let itemObject = response.value as? [String: Any]
                    let statusCode = itemObject?["status"] as? Int ?? 0
                    print(statusCode)
                    
                    if (statusCode == 401) {
                        UserDefaults.standard.removeObject(forKey: "loginToken")
                        self.navigationController?.popToRootViewController(animated: false)
                    }
                    else {
                        let data = itemObject?["data"] as? [String: Any]
                        //print(data)
                        self.nama = data?["nama"] as! String
                        
                        let dompet = data?["dompet"] as?  [String: Any]
                        self.saldo = dompet?["saldo"] as! Double
                        
                        self.tableMenu1.reloadData()
                    }
                
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            return 1
        }
        else if section == 2 {
            return 1
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
            let orangeView = cell1.viewWithTag(1) as! UIView
            let classicTag = cell1.viewWithTag(2) as! UIView
            let orangeFooter = cell1.viewWithTag(3) as! UIView
            let orangeBottomLeft = cell1.viewWithTag(4) as! UIView
            let orangeBottomRight = cell1.viewWithTag(5) as! UIView
            self.labelSaldo = cell1.viewWithTag(6) as! UILabel
            var astraTrackButton = cell1.viewWithTag(7) as! UIButton
            var settingButton = cell1.viewWithTag(8) as! UIButton
            var labelNama = cell1.viewWithTag(9) as! UILabel
            var topupButton = cell1.viewWithTag(10) as! UIButton
            var scanButton = cell1.viewWithTag(11) as! UIButton
            var riwayatButton = cell1.viewWithTag(12) as! UIButton
            
            orangeView.layer.cornerRadius = 14
            classicTag.layer.cornerRadius = 13
            
            orangeFooter.layer.cornerRadius = 14
            orangeFooter.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            orangeBottomLeft.layer.cornerRadius = 14
            orangeBottomLeft.layer.maskedCorners = [.layerMinXMaxYCorner]
            
            orangeBottomRight.layer.cornerRadius = 14
            orangeBottomRight.layer.maskedCorners = [.layerMaxXMaxYCorner]
            
            astraTrackButton.addTarget(self, action: #selector(astraTrackButtonTapped), for: .touchUpInside)
            topupButton.addTarget(self, action: #selector(topupButtonTapped), for: .touchUpInside)
            scanButton.addTarget(self, action: #selector(scanButtonTapped), for: .touchUpInside)
            settingButton.addTarget(self, action: #selector(settingButtonTapped), for: .touchUpInside)
            riwayatButton.addTarget(self, action: #selector(riwayatButtonTapped), for: .touchUpInside)
            
            
            labelNama.text = self.nama
            
            //Currency Formatter
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "id_ID")
            formatter.numberStyle = .currency
            
            //label for saldo
            if let saldoFormatted = formatter.string(from: self.saldo as NSNumber) {
                labelSaldo.text = String(saldoFormatted)
            }
            
            return cell1
        }
        else if indexPath.section == 1 {
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath)
            return cell3
        }
        else if indexPath.section == 2 {
            let cell4 = tableView.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath)
            collectionPromo = cell4.viewWithTag(1) as! UICollectionView
            
            collectionPromo.delegate = self
            collectionPromo.dataSource = self
            
            return cell4
        }
        else {
            let cell5 = tableView.dequeueReusableCell(withIdentifier: "Cell5", for: indexPath)
            collectionProduk = cell5.viewWithTag(1) as! UICollectionView
            
            collectionProduk.delegate = self
            collectionProduk.dataSource = self
            
            return cell5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionPromo {
            return arrayLinkPromosi.count
        }
        else {
            return arrayLinkProduk.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionPromo {
            let collectionCellPromo = collectionView.dequeueReusableCell(withReuseIdentifier: "cellPromosi", for: indexPath)
            var itemPromo = arrayLinkPromosi[indexPath.row]
            
            var imagePromosi = collectionCellPromo.viewWithTag(2) as! UIImageView
            imagePromosi.kf.setImage(with: URL(string: itemPromo))
            imagePromosi.layer.cornerRadius = 15
            
            return collectionCellPromo
        }
        else {
            let collectionCellProduk = collectionView.dequeueReusableCell(withReuseIdentifier: "cellProduk", for: indexPath)
            var itemProduk = arrayLinkProduk[indexPath.row]
            var itemLabelProduk = arrayLabelProduk[indexPath.row]
            
            var imageProduk = collectionCellProduk.viewWithTag(3) as! UIImageView
            var labelProduk = collectionCellProduk.viewWithTag(4) as! UILabel
            labelProduk.text = itemLabelProduk
            imageProduk.kf.setImage(with: URL(string: itemProduk))
            imageProduk.layer.cornerRadius = 15
            
            return collectionCellProduk
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 300
        }
        else if indexPath.section == 1 {
            return 200
        }
        else if indexPath.section == 2 {
            return 250
        }
        else {
            return 200
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionPromo {
            return CGSize.init(width: 348, height: 133)
        }
        else {
            return CGSize.init(width: 118, height: 147)
        }
    }
    
    func updateValueSaldo(text: String) {
        getCurrentUser()
    }

}
