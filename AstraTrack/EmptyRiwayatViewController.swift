//
//  EmptyRiwayatViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 21/12/22.
//

import UIKit

class EmptyRiwayatViewController: UIViewController {
    @IBOutlet weak var emptyRiwayatBackButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emptyRiwayatBackButton.addTarget(self, action: #selector(emptyRiwayatBackButtonTapped), for: .touchUpInside)
    }
    
    @objc func emptyRiwayatBackButtonTapped() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    

}
