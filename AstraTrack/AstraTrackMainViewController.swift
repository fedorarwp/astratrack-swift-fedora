//
//  AstraTrackMainViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 07/12/22.
//

import UIKit
import Alamofire
import Charts

class AstraTrackMainViewController: UIViewController, ChartViewDelegate, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, RiwayatViewControllerDelegate {
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var yearFilter: UITextField!
    
    var kategoriCell:[[String : Any]] = []
    var idKategori: Int = 0
    var judulKategori: String = ""
    var totalPerkategoriArray = [String]()
    var tahunTextfield: UITextField!
    var bulanTextfield: UITextField!
    var arrTahun = ["2021", "2022", "2023"]
    var arrBulan = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    var pickerTahun = UIPickerView()
    var pickerBulan = UIPickerView()
    var saldo: Double = 0
    
    var barChart = BarChartView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleTapDismiss(_:)))
        dismissGesture.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(dismissGesture)
        addButton.addTarget(self, action: #selector(addButtonTapped), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        barChart.delegate = self
        
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        
        requestAllKategoriPerUser()
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
        }
    
    @objc func addButtonTapped() {
        let riwayatViewController = self.storyboard?.instantiateViewController(withIdentifier: "RiwayatViewController") as! RiwayatViewController
        riwayatViewController.delegate = self
        
        self.navigationController?.pushViewController(riwayatViewController, animated: true)
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func requestAllKategoriPerUser() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        AF.request("http://localhost:8080/astratrack/alluserkategori", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let responseData = response.value as? [String: Any]
                    let statusCode = responseData?["status"] as? Int ?? 0

                    if (statusCode == 401) {
                        UserDefaults.standard.removeObject(forKey: "loginToken")
                        self.navigationController?.popToRootViewController(animated: false)
                    } else {
                        let dataArray =  responseData!["data"] as? [[String: Any]]
                        self.kategoriCell = dataArray!
                        self.categoryTableView.reloadData()
                    }

                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            return 1
        }
        else {
            return kategoriCell.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let chartCell = tableView.dequeueReusableCell(withIdentifier: "ChartCell", for: indexPath)
            self.tahunTextfield = chartCell.viewWithTag(1) as! UITextField
            self.bulanTextfield = chartCell.viewWithTag(2) as! UITextField
            var arrayX: [Double] = []
            var newArrayY: [Double] = []
            var entries = [BarChartDataEntry]()
            
            self.tahunTextfield.backgroundColor = NSUIColor(red: 232/255.0, green: 41/255.0, blue: 145/255.0, alpha: 1.0)
            pickerTahun.delegate = self
            self.tahunTextfield.inputView = pickerTahun
            
            self.bulanTextfield.backgroundColor = NSUIColor(red: 232/255.0, green: 41/255.0, blue: 145/255.0, alpha: 1.0)
            pickerBulan.delegate = self
            self.bulanTextfield.inputView = pickerBulan
            
            
//            tahunTextfield.layer.borderColor = UIColor.white.cgColor
            
            barChart.frame = CGRect(x: 70, y: 50, width: 250, height: 230)
            chartCell.addSubview(barChart)
            
//            var newArrayX = ["Cat 1", "Cat 2", "Cat 3", "Cat 4", "Cat 5", "Cat 6"]
//            var arrayY = [20, 12, 36, 7, 89, 49]
            
            for (index, item) in self.kategoriCell.enumerated() {
                var totalValue = item["totalPerkategori"] as? Double
                newArrayY.append(totalValue ?? 0.0)
                arrayX.append(Double(index) + 1.0)
            }
            
            //print(newArrayY)
            
            //total categories
            //for x in 0..<yValues.count
            //entries.append(BarChartDataEntry(x: Double(i), y: yValues[i])
            for (index, _) in arrayX.enumerated() {
                entries.append(BarChartDataEntry(x: Double(arrayX[index]), y: Double(newArrayY[index])))
            }
        
            let set = BarChartDataSet(entries: entries)
            let data = BarChartData(dataSet: set)
            set.colors = ChartColorTemplates.joyful()
            barChart.data = data
            barChart.xAxis.drawGridLinesEnabled = false
            barChart.xAxis.drawLabelsEnabled = true
            barChart.xAxis.drawAxisLineEnabled = false
            barChart.rightAxis.enabled = false
            barChart.leftAxis.enabled = false
            barChart.drawBordersEnabled = false
            //barChart.legend.form = .none //ini buat warna kotak-kotak keterangan
            barChart.legend.enabled = false
            barChart.isUserInteractionEnabled = false
            return chartCell
        }
        else if indexPath.section == 1 {
            let akumulasiCell = tableView.dequeueReusableCell(withIdentifier: "AkumulasiCell", for: indexPath)
            var yellowView = akumulasiCell.viewWithTag(1) as! UIView
            var labelPemasukan = akumulasiCell.viewWithTag(2) as! UILabel
            var labelPengeluaran = akumulasiCell.viewWithTag(3) as! UILabel
            var labelSaldo = akumulasiCell.viewWithTag(4) as! UILabel
            
            yellowView.layer.cornerRadius = 14
            
            var totalPemasukan: Double = 0
            var totalPengeluaran: Double = 0
            for (index, item) in self.kategoriCell.enumerated() {
                //print("index: \(index)")
                var totalValue = item["totalPerkategori"] as? Double
                var riwayatListArray = item["riwayatList"] as! [[String: Any]]
                if !riwayatListArray.isEmpty {
                    var riwayat = riwayatListArray[0] as? [String: Any]
                    var tipeRiwayat = riwayat?["tipe"] as! String
                    //print("tipeRiwayat: \(tipeRiwayat)")
                    if tipeRiwayat == "pemasukan" {
                        totalPemasukan += totalValue!
                    }
                    else if tipeRiwayat == "pengeluaran" {
                        totalPengeluaran += totalValue!
                    }
                    //print(selisih)
                    let mainMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
                }
            }
            
            //Currency Formatter
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "id_ID")
            formatter.numberStyle = .currency
            
            //label for pemasukan
            if let totalPemasukanFormatted = formatter.string(from: totalPemasukan as NSNumber) {
                labelPemasukan.text = String(totalPemasukanFormatted)
            }
            
            //label for pemasukan
            if let totalPengeluaranFormatted = formatter.string(from: totalPengeluaran as NSNumber) {
                labelPengeluaran.text = String(totalPengeluaranFormatted)
            }
            
            //label for selisi
            if let saldoFormatted = formatter.string(from: saldo as NSNumber) {
                labelSaldo.text = String(saldoFormatted)
            }
            
            return akumulasiCell
        }
        else {
            let categoryCell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
            var categoryView = categoryCell.viewWithTag(4) as! UIView
            var categoryIcon = categoryCell.viewWithTag(2) as! UIImageView
            var categoryLabel = categoryCell.viewWithTag(3) as! UILabel
            
            var item = self.kategoriCell[indexPath.row]
            var categoryContent = item["kategori"] as? [String: Any]
            var judulKategori = categoryContent?["judulKategori"] as? String
            var iconKategori = categoryContent?["iconKategori"] as? String
            
            categoryView.layer.cornerRadius = 12
            categoryView.layer.borderWidth = 1
            categoryView.layer.borderColor = UIColor.blue.cgColor
            
            categoryIcon.kf.setImage(with: URL(string: "http://localhost:8080/astratrack/files/\(iconKategori!):.+"))
            
            categoryLabel.text = "\(indexPath.row + 1). \(judulKategori!)"
            
            return categoryCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            print("ini kepencet")
            var item = self.kategoriCell[indexPath.row]
            var categoryContent = item["kategori"] as? [String: Any]
            self.idKategori = categoryContent?["id"] as! Int
            self.judulKategori = categoryContent?["judulKategori"] as! String
            
            let token = UserDefaults.standard.object(forKey: "loginToken") as! String
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            
            AF.request("http://localhost:8080/astratrack/riwayatuserkategori/\(self.idKategori)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        let riwayatResponse = response.value as? [String: Any]
                        
                        let statusCode = riwayatResponse?["status"] as? Int ?? 0
    
                        if (statusCode == 401) {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                            self.navigationController?.popToRootViewController(animated: false)
                        }
                        else {
                            let riwayatKategoriArray = riwayatResponse?["data"] as? [[String: Any]]
                            let kategoriViewController = self.storyboard?.instantiateViewController(withIdentifier: "KategoriViewController") as! KategoriViewController
                            
                            kategoriViewController.riwayatKategoriArray = riwayatKategoriArray!
                            kategoriViewController.judulKategori = self.judulKategori
                            kategoriViewController.idKategori = self.idKategori
                            
                            if riwayatKategoriArray?.isEmpty == true {
                                let emptyViewController = self.storyboard?.instantiateViewController(withIdentifier: "EmptyViewController") as! EmptyViewController
                                self.navigationController?.pushViewController(emptyViewController, animated: true)
                            } else {
                                self.navigationController?.pushViewController(kategoriViewController, animated: true)
                            }
                        }

                    case .failure(let error):
                        print(error)
                    }
                })
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 300
        }
        else if indexPath.section == 1 {
            return 180
        }
        else {
            return 60
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView.tag == 1 {
            return 1
        } else {
            return 1
        }
    }
        
        // returns the number of 'columns' to display.
        func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
            return 1
        }

        // returns the # of rows in each component..
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
            if pickerView.inputView == pickerTahun {
                return arrTahun.count
            }
            else {
                return arrBulan.count
            }
        }

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if pickerView.inputView == pickerTahun {
                return arrTahun[row]
            } else {
                return arrBulan[row]
            }
        }

        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
        {
            if pickerView.inputView == pickerTahun {
                tahunTextfield.text = "\(arrTahun[row])"
            } else {
                bulanTextfield.text = "\(arrBulan[row])"
            }

        }
    
    func updateKategoriCell(array: [[String : Any]]) {
        self.kategoriCell = array
        self.categoryTableView.reloadData()
    }

}
