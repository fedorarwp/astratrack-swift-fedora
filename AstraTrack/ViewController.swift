//
//  ViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 05/12/22.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITextFieldDelegate {
    //var tokenContent: String! = ""
    var seePassword:Bool = true

    @IBOutlet weak var noHpInput: UITextField!
    @IBOutlet weak var pinInput: UITextField!
    @IBOutlet weak var masukButton: UIButton!
    @IBOutlet weak var validasiPin: UILabel!
    @IBOutlet weak var daftarButton: UIButton!
    @IBOutlet weak var validasiNoHp: UILabel!
    @IBOutlet weak var closedEye: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        closedEye.addTarget(self, action: #selector(eyeTapped), for: .touchUpInside)
        
        masukButton.addTarget(self, action: #selector(masukButtonTapped), for: .touchUpInside)
        daftarButton.addTarget(self, action: #selector(daftarButtonTapped), for: .touchUpInside)
        
        noHpInput.delegate = self
        pinInput.delegate = self
        
        validasiPin.isHidden = true
        validasiNoHp.isHidden = true
        
        validasiPin.text = ""
        validasiNoHp.text = ""
        
        masukButton.isEnabled = false
        
        isLoggedIn()
    }
    
    @objc func eyeTapped() {
        let smallEye = UIImage.SymbolConfiguration(scale: .small)
        if seePassword {
            pinInput.isSecureTextEntry = false
            closedEye.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallEye), for: .normal)
        } else {
            pinInput.isSecureTextEntry = true
            closedEye.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallEye), for: .normal)
        }
        seePassword = !seePassword
    }
    
    func isLoggedIn() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as? String
        if token ?? "" != "" {
            let mainMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainMenuViewController")
            self.navigationController?.pushViewController(mainMenuViewController!, animated: true)
        }
    }
    
    @objc func masukButtonTapped() {
        let param = ["noHp":noHpInput.text!, "password":pinInput.text!]
        
        AF.request("http://localhost:8080/astratrack/login", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let itemObject = response.value as? [String: Any]
                    let data = itemObject?["data"] as? [String: Any]
                    let token = data?["token"] as? String
                    let message = itemObject?["message"] as? String
                    
                    //save token for the whole session
                    UserDefaults.standard.setValue(token, forKey: "loginToken")
                    UserDefaults.standard.synchronize()

                    if (token ?? "" != "") {
                        print(token!)
                        print(message!)
                        let alertController = UIAlertController(title: "Log in berhasil!", message: "Akun anda valid", preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "Masuk", style: .default, handler: {
                            action in
                            let mainMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainMenuViewController")
                            self.navigationController?.pushViewController(mainMenuViewController!, animated: true)
                        }));
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else {
                        print(message!)
                        let alertController = UIAlertController(title: "Log in gagal!", message: "Masukkan akun yang terdaftar", preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "Saya mengerti", style: .default, handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func daftarButtonTapped() {
        let registerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController")
        
        self.navigationController?.pushViewController(registerViewController!, animated: true)
    }
    
    
    
    @IBAction func pinChange(_ sender: Any) {
        if let pin = pinInput.text {
            if let errorMessage = invalidPassword(pin) {
                validasiPin.text = errorMessage
                validasiPin.isHidden = false
            }
            else {
                validasiPin.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidPassword(_ value: String) -> String? {
        if (value.count != 6) {
            return "Panjang PIN harus 6"
        }
        if !containsDigit(value) {
            return "PIN harus berupa angka"
        }
//        if !containsLowerCase(value) {
//            return "Harus mengandung huruf kecil"
//        }
//        if !containsUpperCase(value) {
//            return "Harus mengandung huruf besar"
//        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
//    func containsLowerCase(_ value: String) -> Bool {
//        let regex = ".*[a-z].*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        return predicate.evaluate(with: value)
//    }
//
//    func containsUpperCase(_ value: String) -> Bool {
//        let regex = ".*[A-Z].*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        return predicate.evaluate(with: value)
//    }
    
    func checkValidation() {
        if (validasiPin.isHidden && validasiNoHp.isHidden && validasiPin.text != "" && validasiNoHp.text != "") {
            masukButton.isEnabled = true
        }
        else {
            masukButton.isEnabled = false
        }
    }
    
    @IBAction func phoneNumberChange(_ sender: Any) {
        
        if let noHp = noHpInput.text {
            if let errorMessage = invalidNoHp(noHp) {
                validasiNoHp.text = errorMessage
                validasiNoHp.isHidden = false
            }
            else {
                validasiNoHp.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidNoHp(_ value: String) -> String? {
        if (value.count < 10) {
            return "Panjang Nomor HP minimal 10"
        }
        if !containsDigit(value) {
            return "Nomor HP harus berupa angka"
        }
        return nil
    }

    
}

