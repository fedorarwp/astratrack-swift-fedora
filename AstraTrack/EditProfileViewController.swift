//
//  EditProfileViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 28/12/22.
//

import UIKit
import Alamofire

class EditProfileViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var namaTextField: UITextField!
    @IBOutlet weak var simpanButton: UIButton!
    @IBOutlet weak var validasiEditNama: UILabel!
    @IBOutlet weak var validasiEditEmail: UILabel!
    @IBOutlet weak var editProfileBackButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var editProfileImage: UIView!
    @IBOutlet weak var validasiEditPassword: UILabel!
    @IBOutlet weak var closedEye: UIButton!
    
    var nama: String!
    var email: String!
    var seePassword: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCurrentUser()
        
        validasiEditNama.isHidden = true
        validasiEditEmail.isHidden = true
        validasiEditPassword.isHidden = true
        simpanButton.isEnabled = false
        passwordTextField.text = ""
        
        closedEye.addTarget(self, action: #selector(eyeTapped), for: .touchUpInside)
        simpanButton.addTarget(self, action: #selector(simpanButtonTapped), for: .touchUpInside)
        editProfileBackButton.addTarget(self, action: #selector(editProfileBackButtonTapped), for: .touchUpInside)
        
        editProfileImage.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        editProfileImage.layer.cornerRadius = 20
    }
    
    func getCurrentUser() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        AF.request("http://localhost:8080/astratrack/currentuser", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let itemObject = response.value as? [String: Any]
                    let statusCode = itemObject?["status"] as? Int ?? 0
                    
                    if (statusCode == 401) {
                        UserDefaults.standard.removeObject(forKey: "loginToken")
                        self.navigationController?.popToRootViewController(animated: false)
                    } else {
                        let data = itemObject?["data"] as? [String: Any]
                        self.nama = data?["nama"] as! String
                        self.email = data?["email"] as! String
                        
                        self.namaTextField.text = self.nama!
                        self.emailTextField.text = self.email!
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func editProfileBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func eyeTapped() {
        let smallEye = UIImage.SymbolConfiguration(scale: .small)
        if seePassword {
            passwordTextField.isSecureTextEntry = false
            closedEye.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallEye), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            closedEye.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallEye), for: .normal)
        }
        seePassword = !seePassword
    }
    
    @IBAction func editNamaChange(_ sender: Any) {
        if let nama = namaTextField.text {
            if let errorMessage = invalidEditNama(nama) {
                validasiEditNama.text = errorMessage
                validasiEditNama.isHidden = false
            } else {
                validasiEditNama.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidEditNama(_ value: String) -> String? {
        if (value.count < 1) {
            return "Nama tidak boleh kosong"
        }
        return nil
    }
    
    @IBAction func editEmailChange(_ sender: Any) {
        if let email = emailTextField.text {
            if let errorMessage = invalidEditEmail(email) {
                validasiEditEmail.text = errorMessage
                validasiEditEmail.isHidden = false
            }
            else {
                validasiEditEmail.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidEditEmail(_ value: String) -> String? {
        if !emailFormat(value) {
            return "Format email tidak sesuai"
        }
        return nil
    }
    
    func emailFormat(_ value: String) -> Bool {
        let regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
        + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    @IBAction func editPasswordChange(_ sender: Any) {
        if let password = passwordTextField.text {
            if let errorMessage = invalidEditPassword(password) {
                validasiEditPassword.text = errorMessage
                validasiEditPassword.isHidden = false
            }
            else {
                validasiEditPassword.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidEditPassword(_ value: String) -> String? {
        if (value != "") {
            if (value.count != 6) {
                return "Panjang PIN harus 6"
            }
        }
        
        if !containsDigit(value) {
            return "PIN harus berupa angka"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidation() {
        if (validasiEditNama.isHidden && validasiEditEmail.isHidden && validasiEditPassword.isHidden) {
            simpanButton.isEnabled = true
        }
        else {
            simpanButton.isEnabled = false
        }
    }
    
    @objc func simpanButtonTapped() {
        let alertController = UIAlertController(title: "Anda yakin?", message: "Apakah anda yakin akan mengubah data diri anda?", preferredStyle: .alert);
        let yesConfirm = UIAlertAction(title: "Yakin", style: .default, handler: {
            action in
            let token = UserDefaults.standard.object(forKey: "loginToken") as! String
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            let param = ["nama": self.namaTextField.text!, "email": self.emailTextField.text!, "password": self.passwordTextField.text!]
            
            AF.request("http://localhost:8080/astratrack/edituser", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        
                        let itemObject = response.value as? [String: Any]
                        let success = itemObject?["success"] as! Bool
                        let data = itemObject?["data"] as? [String: Any]
                        
                        print(data)
                        
                        if success {
                            if self.passwordTextField.text == "" {
                                let alertController = UIAlertController(title: "Berhasil!", message: "Perubahan data berhasil disimpan", preferredStyle: .alert);
                                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                                    action in
                                    self.navigationController?.popViewController(animated: true)
                                }));
                                self.present(alertController, animated: true, completion: nil)
                            } else {
                                UserDefaults.standard.removeObject(forKey: "loginToken")
                                let alertController = UIAlertController(title: "Berhasil!", message: "Perubahan data berhasil disimpan", preferredStyle: .alert);
                                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                                    action in
                                    let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
                                    self.navigationController?.pushViewController(loginViewController!, animated: true)
                                }));
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                })
        })
        
        let noConfirm = UIAlertAction(title: "Tidak", style: .default, handler: nil)
        alertController.addAction(noConfirm)
        alertController.addAction(yesConfirm);
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}
