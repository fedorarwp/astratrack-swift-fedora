//
//  KeluarViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 03/01/23.
//

import UIKit
import Alamofire

class KeluarViewController: UIViewController {

    @IBOutlet weak var nantiButton: UIButton!
    @IBOutlet weak var yaButton: UIButton!
    
    var delegate: GoToLogInPageDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nantiButton.layer.borderColor = UIColor.blue.cgColor
        yaButton.addTarget(self, action: #selector(yaButtonTapped), for: .touchUpInside)
    }
    
    @objc func yaButtonTapped() {
        self.dismiss(animated: false)
        
        UserDefaults.standard.removeObject(forKey: "loginToken")
        delegate?.pushToLogInPage()
    }
    
}
