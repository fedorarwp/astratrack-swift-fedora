//
//  KategoriViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 12/12/22.
//

import UIKit
import Alamofire

class KategoriViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var riwayatKategoriTable: UITableView!
    @IBOutlet weak var labelNamaKategori: UILabel!
    @IBOutlet weak var labelTotalKategori: UILabel!
    @IBOutlet weak var kategoriBackButton: UIButton!
    @IBOutlet weak var badgeTotal: UIView!
    @IBOutlet weak var orangeBackground: UIImageView!
    
    var riwayatKategoriArray: [[String: Any]] = []
    var judulKategori: String = ""
    var idKategori: Int = 0
    var totalPerkategori: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(riwayatKategoriArray)

        kategoriBackButton.addTarget(self, action: #selector(kategoriBackButtonTapped), for: .touchUpInside)
        labelNamaKategori.text = judulKategori
        riwayatKategoriTable.delegate = self
        riwayatKategoriTable.dataSource = self
        
        requestTotalPerkategori()
        
        badgeTotal.layer.cornerRadius = 15
        orangeBackground.layer.cornerRadius = 15
    }
    
    @objc func kategoriBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func requestTotalPerkategori() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/astratrack/kategoriperuser/\(self.idKategori)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    let itemObject = response.value as? [String: Any]
                    let statusCode = itemObject?["status"] as? Int ?? 0
                    
                    if (statusCode == 401) {
                        UserDefaults.standard.removeObject(forKey: "loginToken")
                    }
                    else {
                        self.totalPerkategori = itemObject?["totalPerkategori"] as! Double
                        
                        //Currency Formatter
                        let formatter = NumberFormatter()
                        formatter.locale = Locale(identifier: "id_ID")
                        formatter.numberStyle = .currency
                        
                        //label for total per kategori
                        if let totalKategoriFormatted = formatter.string(from: self.totalPerkategori as NSNumber) {
                            self.labelTotalKategori.text = String(totalKategoriFormatted)
                        }
                        
                    }
                case .failure(let error):
                    print(error)
                }
            })
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        riwayatKategoriArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let riwayatKategoriCell = tableView.dequeueReusableCell(withIdentifier: "RiwayatKategoriCell", for: indexPath)
        var iconRiwayatKategori = riwayatKategoriCell.viewWithTag(1) as! UIImageView
        var namaRiwayatKategori = riwayatKategoriCell.viewWithTag(2) as! UILabel
        var tanggalWaktuRiwayatKategori = riwayatKategoriCell.viewWithTag(3) as! UILabel
        var totalRiwayatKategori = riwayatKategoriCell.viewWithTag(4) as! UILabel
        
        var item = self.riwayatKategoriArray[indexPath.row]
        var iconRiwayat = item["iconRiwayat"] as? String
        var namaRiwayat = item["namaRiwayat"] as! String
        var date = item["date"] as! String
        var time = item["time"] as! String
        var total = item["total"] as! Double
        
        iconRiwayatKategori.kf.setImage(with: URL(string: "http://localhost:8080/astratrack/files/\(iconRiwayat!):.+"))
        namaRiwayatKategori.text = namaRiwayat
        tanggalWaktuRiwayatKategori.text = "\(date) • \(time)"
        
        //Currency Formatter
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.numberStyle = .currency
        
        //label for total for each riwayat kategori
        if let totalRiwayatKategoriFormatted = formatter.string(from: total as NSNumber) {
            totalRiwayatKategori.text = String(totalRiwayatKategoriFormatted)
        }
        
        return riwayatKategoriCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
