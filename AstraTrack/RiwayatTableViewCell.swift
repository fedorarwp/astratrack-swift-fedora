//
//  RiwayatTableViewCell.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 15/12/22.
//

import UIKit

class RiwayatTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource  {

    @IBOutlet weak var kategoriTextField: UITextField!
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var symbolTotal: UILabel!
    @IBOutlet weak var tanggalWaktuRiwayat: UILabel!
    @IBOutlet weak var namaRiwayat: UILabel!
    @IBOutlet weak var iconRiwayat: UIImageView!
    
    var kategoriDropdown = UIPickerView()
    var kategoriArray = [String]()
    var idKategoriArray = [Int]()
    var doneButtonTappedCallback: ((Int) -> ())?
    var idKategori: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Toolbar
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneFunction))
        
        let barAccessory = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        barAccessory.barStyle = .default
        barAccessory.isTranslucent = false
        barAccessory.items = [btnDone]
        self.kategoriTextField.inputAccessoryView = barAccessory
        
        //pickerview
        kategoriDropdown.delegate = self
        print("Kategori Array\(kategoriArray)")
        print("Id Kategori Array\(idKategoriArray)")
        
        self.kategoriTextField.inputView = kategoriDropdown
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }

    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }

    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return kategoriArray.count-1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return kategoriArray[row+1]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        kategoriTextField.text = "   \(kategoriArray[row+1])"
        idKategori = idKategoriArray[row+1]
    }
    
    @objc func doneFunction() {
        //print("ini button done")
        doneButtonTappedCallback?(idKategori)
    }
    
    
}
