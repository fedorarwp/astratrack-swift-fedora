//
//  SuksesBayarViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 08/01/23.
//

import UIKit

class SuksesBayarViewController: UIViewController {

    @IBOutlet weak var berandaButton: UIButton!
    @IBOutlet weak var labelTanggalWaktuTransaksi: UILabel!
    @IBOutlet weak var labelNamaPembayaran: UILabel!
    @IBOutlet weak var labelJumlahTransaksi: UILabel!
    
    var tanggalWaktuTransaksi: String = ""
    var namaPembayaran: String = ""
    var jumlahTransaksi: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelTanggalWaktuTransaksi.text = tanggalWaktuTransaksi
        labelNamaPembayaran.text = namaPembayaran
        labelJumlahTransaksi.text = jumlahTransaksi
        
        berandaButton.addTarget(self, action: #selector(berandaButtonTapped), for: .touchUpInside)
    }
    
    @objc func berandaButtonTapped() {
        NotificationCenter.default.post(name: Notification.Name("UpdateSaldoAPMainMenu"), object: nil)
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
    }


}
