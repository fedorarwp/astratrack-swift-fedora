//
//  EmptyViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 21/12/22.
//

import UIKit

class EmptyViewController: UIViewController {

    @IBOutlet weak var emptyBackButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        emptyBackButton.addTarget(self, action: #selector(emptyBackButtonTapped), for: .touchUpInside)
    }
    

    @objc func emptyBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }

}
