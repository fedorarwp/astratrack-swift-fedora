//
//  BayarViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 12/12/22.
//

import UIKit
import Alamofire

class BayarViewController: UIViewController {

    @IBOutlet weak var totalPembayaranInput: UITextField!
    @IBOutlet weak var namaPembayaranInput: UITextField!
    @IBOutlet weak var validasiNamaPembayaran: UILabel!
    @IBOutlet weak var validasiTotalPembayaran: UILabel!
    @IBOutlet weak var bayarButton: UIButton!
    @IBOutlet weak var bayarBackButton: UIButton!
    
    var saldo: Double = 0.0
    var doubleTotal: String = ""
    var namaPembayaran: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bayarButton.addTarget(self, action: #selector(bayarButtonTapped), for: .touchUpInside)
        bayarBackButton.addTarget(self, action: #selector(bayarBackButtonTapped), for: .touchUpInside)
        bayarButton.isEnabled = false
        validasiNamaPembayaran.isHidden = true
        validasiTotalPembayaran.isHidden = true
    }
    
    @objc func bayarBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func bayarButtonTapped() {
        let confirmBayarViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmBayarViewController") as! ConfirmBayarViewController
        
        //delegate page to update saldo
        print(self.saldo)
        doubleTotal = self.totalPembayaranInput.text!
        namaPembayaran = self.namaPembayaranInput.text!
        confirmBayarViewController.saldo = self.saldo
        confirmBayarViewController.totalTransaksi = Double(self.doubleTotal)!
        confirmBayarViewController.namaPembayaran = self.namaPembayaran
        print(Double(doubleTotal)!)
        
        self.navigationController?.pushViewController(confirmBayarViewController, animated: true)
    }
    
    
    @IBAction func totalPembayaranChange(_ sender: Any) {
        if let totalPembayaran = totalPembayaranInput.text {
            if let errorMessage = invalidTotalPembayaran(totalPembayaran) {
                validasiTotalPembayaran.text = errorMessage
                validasiTotalPembayaran.isHidden = false
            }
            else {
                validasiTotalPembayaran.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidTotalPembayaran(_ value: String) -> String? {
        if !angkaFormat(value) {
            return "Total harus angka"
        }
        return nil
    }
    
    
    func angkaFormat(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidation() {
        if (validasiTotalPembayaran.isHidden) {
            bayarButton.isEnabled = true
        }
        else {
            bayarButton.isEnabled = false
        }
    }
    
    
}


