//
//  RegisterViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 05/12/22.
//

import Alamofire
import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {

    var seePassword:Bool = true
    
    @IBOutlet weak var daftarButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var noHpInput: UITextField!
    @IBOutlet weak var namaInput: UITextField!
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var pinInput: UITextField!
    @IBOutlet weak var tanggalLahirInput: UITextField!
    
    @IBOutlet weak var validasiNoHp: UILabel!
    @IBOutlet weak var validasiEmail: UILabel!
    @IBOutlet weak var validasiPin: UILabel!
    @IBOutlet weak var validasiNamaLengkap: UILabel!
    @IBOutlet weak var closedEye: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        let datePicker = UIDatePicker()
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleTapDismiss(_:)))
        
        self.view.addGestureRecognizer(dismissGesture)
        
        closedEye.addTarget(self, action: #selector(eyeTapped), for: .touchUpInside)
        
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        daftarButton.addTarget(self, action: #selector(daftarButtonTapped), for: .touchUpInside)
        
        noHpInput.delegate = self
        namaInput.delegate = self
        emailInput.delegate = self
        tanggalLahirInput.delegate = self
        pinInput.delegate = self
        
        validasiPin.isHidden = true
        validasiNoHp.isHidden = true
        validasiEmail.isHidden = true
        validasiNamaLengkap.isHidden = true
        
        validasiPin.text = ""
        validasiNoHp.text = ""
        validasiEmail.text = ""
        validasiNamaLengkap.text = ""
        
        daftarButton.isEnabled = false
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChange(datePicker:)), for: UIControl.Event.valueChanged)
        
        datePicker.frame.size = CGSize(width: 0, height: 300)
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.maximumDate = Date()
        tanggalLahirInput.inputView = datePicker
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
        }
    
    @objc func eyeTapped() {
        let smallEye = UIImage.SymbolConfiguration(scale: .small)
        if seePassword {
            pinInput.isSecureTextEntry = false
            closedEye.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallEye), for: .normal)
        } else {
            pinInput.isSecureTextEntry = true
            closedEye.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallEye), for: .normal)
        }
        seePassword = !seePassword
    }
    
    @objc func loginButtonTapped() {
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
        
        self.navigationController?.pushViewController(loginViewController!, animated: true)
    }
    
    @objc func daftarButtonTapped() {
        let param = ["noHp":noHpInput.text!, "nama": namaInput.text!, "email": emailInput.text!, "tanggalLahir": tanggalLahirInput.text!, "password":pinInput.text!]
        
        AF.request("http://localhost:8080/astratrack/register", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let itemObject = response.value as? [String: Any]
                    let data = itemObject?["data"] as? [String: Any]
                    let message = itemObject?["message"] as! String
                    
                    print(data)
                    
                    if (data != nil) {
                        let alertController = UIAlertController(title: "Registrasi berhasil!", message: "Silakan log in", preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "Log in", style: .default, handler: {
                            action in
                            let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
                            self.navigationController?.pushViewController(loginViewController!, animated: true)
                        }));
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else {
                        let alertController = UIAlertController(title: "Registrasi gagal!", message: message, preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "Mengerti", style: .default, handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func dateChange(datePicker: UIDatePicker) {
        tanggalLahirInput.text = formatDate(date: datePicker.date)
    }
    
    func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.string(from: date)
    }
    
    @IBAction func noHpChange(_ sender: Any) {
        if let noHp = noHpInput.text {
            if let errorMessage = invalidNoHp(noHp) {
                validasiNoHp.text = errorMessage
                validasiNoHp.isHidden = false
            } else {
                validasiNoHp.isHidden = true
            }
        }
        
        checkValidation()
    }
    
    func invalidNoHp(_ value: String) -> String? {
        if (value.count < 10) {
            return "Panjang Nomor HP minimal 10"
        }
        if !containsDigit(value) {
            return "Nomor HP harus berupa angka"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    @IBAction func namaLengkapChange(_ sender: Any) {
        if let namaLengkap = namaInput.text {
            if let errorMessage = invalidNamaLengkap(namaLengkap) {
                validasiNamaLengkap.text = errorMessage
                validasiNamaLengkap.isHidden = false
            }
            else {
                validasiNamaLengkap.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidNamaLengkap(_ value: String) -> String? {
        if (value.count < 1) {
            return "Nama tidak boleh kosong"
        }
        return nil
    }
    
    @IBAction func emailChange(_ sender: Any) {
        if let email = emailInput.text {
            if let errorMessage = invalidEmail(email) {
                validasiEmail.text = errorMessage
                validasiEmail.isHidden = false
            }
            else {
                validasiEmail.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidEmail(_ value: String) -> String? {
        if !emailFormat(value) {
            return "Format email tidak sesuai"
        }
        return nil
    }
    
    func emailFormat(_ value: String) -> Bool {
        let regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
        + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    
    @IBAction func pinChange(_ sender: Any) {
        if let pin = pinInput.text {
            if let errorMessage = invalidPin(pin) {
                validasiPin.text = errorMessage
                validasiPin.isHidden = false
            }
            else {
                validasiPin.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidPin(_ value: String) -> String? {
        if (value.count != 6) {
            return "Panjang PIN harus 6"
        }
        if !containsDigit(value) {
            return "PIN harus berupa angka"
        }
        return nil
    }
    
    
    func checkValidation() {
        if (validasiPin.isHidden && validasiNoHp.isHidden && validasiEmail.isHidden && validasiNamaLengkap.isHidden && pinInput.text != "" && noHpInput.text != "" && emailInput.text != "" && namaInput.text != "") {
            daftarButton.isEnabled = true
        }
        else {
            daftarButton.isEnabled = false
        }
    }
    
    
}
