//
//  TopUpViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 12/12/22.
//

import UIKit
import Alamofire

protocol TopUpViewControllerDelegate{
    func updateValueSaldo (text: String)
}

class TopUpViewController: UIViewController {

    @IBOutlet weak var totalInput: UITextField!
    @IBOutlet weak var topupButton: UIButton!
    @IBOutlet weak var validasiJumlah: UILabel!
    @IBOutlet weak var topupBackButton: UIButton!
    
    var delegate:TopUpViewControllerDelegate?
    var saldo: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topupBackButton.addTarget(self, action: #selector(topupBackButtonTapped), for: .touchUpInside)
        topupButton.addTarget(self, action: #selector(topupButtonTapped), for: .touchUpInside)
        
        topupButton.isEnabled = false
        validasiJumlah.isHidden = true
    }
    
    @objc func topupBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func topupButtonTapped() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["total":totalInput.text!, "tipe": "pemasukan", "status": "Berhasil", "namaRiwayat": "Top Up", "iconRiwayat": "logo-top-up.png"]
        
        //request post create top up
        AF.request("http://localhost:8080/astratrack/riwayat/1", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let itemObject = response.value as? [String: Any]
                    let data = itemObject?["data"] as? [String: Any]
                    
                    print(data)
                    
                    //request get saldo
                    AF.request("http://localhost:8080/astratrack/currentuser", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                        .responseJSON(completionHandler: { response in
                            switch response.result {
                            case .success:
                                let itemObject = response.value as? [String: Any]
                                let statusCode = itemObject?["status"] as? Int ?? 0
                                
                                if (statusCode == 401) {
                                    UserDefaults.standard.removeObject(forKey: "loginToken")
                                    self.navigationController?.popToRootViewController(animated: false)
                                } else {
                                    let data = itemObject?["data"] as? [String: Any]
                                    let dompet = data?["dompet"] as?  [String: Any]
                                    self.saldo = dompet?["saldo"] as! Double
                                    
                                    //delegate page to update saldo
                                    print(self.saldo)
                                    let alertController = UIAlertController(title: "Top Up Berhasil!", message: "Saldo berhasil ditambahkan", preferredStyle: .alert);
                                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                                        action in
                                        self.delegate?.updateValueSaldo(text: String(self.saldo))
                                        self.navigationController?.popViewController(animated: true)
                                    }));
                                    self.present(alertController, animated: true, completion: nil)
                                }
                                
                                
                            case .failure(let error):
                                print(error)
                            }
                        })
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    
    @IBAction func jumlahChange(_ sender: Any) {
        if let jumlah = totalInput.text {
            if let errorMessage = invalidJumlah(jumlah) {
                validasiJumlah.text = errorMessage
                validasiJumlah.isHidden = false
            }
            else {
                validasiJumlah.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidJumlah(_ value: String) -> String? {
        if !jumlahFormat(value) {
            return "Jumlah top up harus angka"
        }
        return nil
    }
    
    func jumlahFormat(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidation() {
        if (validasiJumlah.isHidden) {
            topupButton.isEnabled = true
        }
        else {
            topupButton.isEnabled = false
        }
    }
    

}
