//
//  RiwayatViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 08/12/22.
//

import UIKit
import Alamofire

protocol RiwayatViewControllerDelegate{
    func updateKategoriCell (array: [[String : Any]])
}

class RiwayatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var riwayatBackButton: UIButton!
    @IBOutlet weak var riwayatTable: UITableView!
    
    var riwayatArray:[[String : Any]] = []
    var kategoriArray = [String]()
    var idKategoriArray = [Int]()
    var judulKategoriForEachRiwayat = [Int: String]()
    var delegate:RiwayatViewControllerDelegate?
    var kategoriCell:[[String : Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        riwayatTable.delegate = self
        riwayatTable.dataSource = self
        
        riwayatBackButton.addTarget(self, action: #selector(riwayatBackButtonTapped), for: .touchUpInside)
        
        requestAllRiwayat()
        requestAllKategori()
    }
    
    @objc func riwayatBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return riwayatArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let riwayatCell = tableView.dequeueReusableCell(withIdentifier: "RiwayatCell", for: indexPath) as! RiwayatTableViewCell
        
        riwayatCell.kategoriArray = self.kategoriArray
        riwayatCell.idKategoriArray = self.idKategoriArray
        
        var item = self.riwayatArray[indexPath.row]
        var iconRiwayatDb = item["iconRiwayat"] as? String
        var namaRiwayatDb = item["namaRiwayat"] as! String
        var total = item["total"] as! Double
        var status = item["status"] as! String
        var tipe = item["tipe"] as! String
        var idRiwayat = item["id"] as! Int
        var date = item["date"] as! String
        var time = item["time"] as! String
        
        //date formatter
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        
        let dateFormatted = dateFormatterGet.date(from: date)
        riwayatCell.tanggalWaktuRiwayat.text = "\(dateFormatterPrint.string(from: dateFormatted!)) • \(time)"
        
        //print(self.idRiwayat)
        
        riwayatCell.iconRiwayat.kf.setImage(with: URL(string: "http://localhost:8080/astratrack/files/\(iconRiwayatDb!):.+"))
        riwayatCell.namaRiwayat.text = namaRiwayatDb
        riwayatCell.Status.text = status
        
        //riwayatCell.tanggalWaktuRiwayat.text = "\(date) • \(time)"
        
        //Currency Formatter
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.numberStyle = .currency
        
        //label for total for each riwayat
        if let totalRiwayatFormatted = formatter.string(from: total as NSNumber) {
            riwayatCell.total.text = String(totalRiwayatFormatted)
        }
        
        //nilai awal kategorinya di sini
        if self.judulKategoriForEachRiwayat.count == self.riwayatArray.count {
            var downView = UIImageView()
            var downImage = UIImage(named: "ExpandArrow")
            
            downView.image = downImage
            riwayatCell.kategoriTextField.rightView = downView
            
            if tipe == "pemasukan" {
                riwayatCell.symbolTotal.text = "+"
                riwayatCell.kategoriTextField.text = "   Pemasukan"
                riwayatCell.kategoriTextField.isEnabled = false
                riwayatCell.kategoriTextField.rightViewMode = .never
            }
            else {
                riwayatCell.symbolTotal.text = "-"
                riwayatCell.kategoriTextField.text = "   \(self.judulKategoriForEachRiwayat[idRiwayat]!)"
                riwayatCell.kategoriTextField.isEnabled = true
                riwayatCell.kategoriTextField.rightViewMode = .always
            }
            riwayatCell.kategoriTextField.layer.cornerRadius = 10
        }
        
        riwayatCell.doneButtonTappedCallback = { id in
            let token = UserDefaults.standard.object(forKey: "loginToken") as! String
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            let param = ["id": id]
            
            AF.request("http://localhost:8080/astratrack/editkategori/\(idRiwayat)", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        
                        let itemObject = response.value as? [String: Any]
                        let data = itemObject?["data"] as? [String: Any]
                        
                        AF.request("http://localhost:8080/astratrack/alluserkategori", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
                            .responseJSON(completionHandler: { response in
                                switch response.result {
                                case .success:
                                    
                                    let responseData = response.value as? [String: Any]
                                    let statusCode = responseData?["status"] as? Int ?? 0

                                    if (statusCode == 401) {
                                        UserDefaults.standard.removeObject(forKey: "loginToken")
                                        self.navigationController?.popToRootViewController(animated: false)
                                    } else {
                                        let dataArray =  responseData!["data"] as? [[String: Any]]
                                        self.kategoriCell = dataArray!
                                        
                                        self.delegate?.updateKategoriCell(array: self.kategoriCell)
                                    }

                                case .failure(let error):
                                    print(error)
                                }
                            })
                        
                        //print(data)
                        
                    case .failure(let error):
                        print(error)
                    }
                })
            
            self.view.endEditing(true)
            print("ini dari callback")
        }
        
        return riwayatCell
    }

    
    func requestAllRiwayat() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/astratrack/riwayatuser", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    let allRiwayat = response.value as? [String: Any]
                    
                    let statusCode = allRiwayat?["status"] as? Int ?? 0

                    if (statusCode == 401) {
                        UserDefaults.standard.removeObject(forKey: "loginToken")
                        self.navigationController?.popToRootViewController(animated: false)
                    }
                    else {
                        let dataArray = allRiwayat?["data"] as! [[String: Any]]
                        self.riwayatArray = dataArray
                        //self.riwayatTable.reloadData()
                        
                        //print(riwayatKategoriArray)
                        if dataArray.isEmpty == true {
                            let emptyRiwayatViewController = self.storyboard?.instantiateViewController(withIdentifier: "EmptyRiwayatViewController") as! EmptyRiwayatViewController
                            self.navigationController?.pushViewController(emptyRiwayatViewController, animated: true)
                        }
                        else {
                            for eachRiwayatId in self.riwayatArray {
                                let idRiwayat = eachRiwayatId["id"] as! Int
                                self.requestKategoriForEachRiwayat(idRiwayat: idRiwayat)
                            }
                        }
                    }
                    

                case .failure(let error):
                    print(error)
                }
            })
    }
    
    
    func requestAllKategori() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/astratrack/kategori", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    let allKategori = response.value as? [String: Any]
                    let dataKategori = allKategori?["data"] as! [[String: Any]]
                    for eachKategori in dataKategori {
                        let judulKategori = eachKategori["judulKategori"] as! String
                        let idKategori = eachKategori["id"] as! Int
                        self.kategoriArray.append(judulKategori)
                        self.idKategoriArray.append(idKategori)
                    }
//                    self.kategoriArray.remove(at: 0)
//                    self.idKategoriArray.remove(at: 0)
                    self.riwayatTable.reloadData()

                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func requestKategoriForEachRiwayat(idRiwayat: Int) {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        

        AF.request("http://localhost:8080/astratrack/kategoriforriwayat/\(idRiwayat)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    let contentKategori = response.value as? [String: Any]
                    let dataKategori = contentKategori?["data"] as? [String: Any]
                    let judulKategori = dataKategori?["judulKategori"] as! String
                    self.judulKategoriForEachRiwayat[idRiwayat] = judulKategori
                    print(self.judulKategoriForEachRiwayat)
                    
                    self.riwayatTable.reloadData()

                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

}
