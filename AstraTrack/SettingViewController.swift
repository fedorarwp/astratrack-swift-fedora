//
//  SettingViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 12/12/22.
//
import Alamofire
import UIKit

protocol GoToLogInPageDelegate {
    func pushToLogInPage()
}

class SettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GoToLogInPageDelegate {
    @IBOutlet weak var profilBackButton: UIButton!
    @IBOutlet weak var profilTable: UITableView!
    @IBOutlet weak var layerView: UIView!
    
    var nama: String = ""
    var noHp: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profilBackButton.addTarget(self, action: #selector(profilBackButtonTapped), for: .touchUpInside)

        profilTable.delegate = self
        profilTable.dataSource = self
        
        layerView.isHidden = true
    }
    
    @objc func getCurrentUser() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as? String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token ?? "")"]
        AF.request("http://localhost:8080/astratrack/currentuser", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let itemObject = response.value as? [String: Any]
                    let statusCode = itemObject?["status"] as? Int ?? 0
                    
                    if (statusCode == 401) {
                        UserDefaults.standard.removeObject(forKey: "loginToken")
                        self.navigationController?.popToRootViewController(animated: false)
                    } else {
                        let data = itemObject?["data"] as? [String: Any]
                        self.nama = data?["nama"] as! String
                        self.noHp = data?["noHp"] as! String
                        
                        //print(self.nama)
                        
                        self.profilTable.reloadData()
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func profilBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if section == 1 {
            return 1
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
            getCurrentUser()
            
            var namaLabel = cell1.viewWithTag(1) as! UILabel
            var noHpLabel = cell1.viewWithTag(2) as! UILabel
            var fotoProfil = cell1.viewWithTag(3) as! UIImageView
            namaLabel.text = self.nama
            noHpLabel.text = self.noHp
            fotoProfil.layer.cornerRadius = 10
            
            
            return cell1
        }
        if indexPath.section == 1 {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath)
            
            return cell2
        }
        else {
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath)
            
            return cell3
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 120
        }
        else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let editProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController")
            
            self.navigationController?.pushViewController(editProfileViewController!, animated: true)
        }
        if indexPath.section == 2 {
            let keluarViewController = self.storyboard?.instantiateViewController(withIdentifier: "KeluarViewController") as! KeluarViewController
                    layerView.isHidden = false
            keluarViewController.providesPresentationContextTransitionStyle = true;
            keluarViewController.definesPresentationContext = true;
            keluarViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

            keluarViewController.delegate = self
            self.present(keluarViewController, animated: true, completion: {
            keluarViewController.view.superview?.isUserInteractionEnabled = true
            keluarViewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                self.layerView.isHidden = false
            })
            
            keluarViewController.nantiButton.addTarget(self, action: #selector(nantiButtonTapped), for: .touchUpInside)
            
        }
    }
    
    @objc func nantiButtonTapped(){
            self.dismiss(animated: true)
            self.layerView.isHidden = true
        }
    
    @objc func dismissOnTapOutsideModal(){
        self.dismiss(animated: true, completion: nil)
        layerView.isHidden = true
        }
    
    func pushToLogInPage() {
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
        self.navigationController?.pushViewController(loginViewController!, animated: true)
    }
    
}
