//
//  ConfirmBayarViewController.swift
//  AstraTrack
//
//  Created by Fedora Widijanto on 06/01/23.
//

import Alamofire
import UIKit

class ConfirmBayarViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var bayarButton: UIButton!
    @IBOutlet weak var labelTotalTransaksi: UILabel!
    @IBOutlet weak var labelTotalBayar: UILabel!
    @IBOutlet weak var labelSaldoDompet: UILabel!
    @IBOutlet weak var labelNamaPembayaran: UILabel!
    @IBOutlet weak var confirmBayarBackButton: UIButton!
    @IBOutlet weak var labelTanggalWaktu: UILabel!
    @IBOutlet weak var kategoriTextField: UITextField!
    @IBOutlet weak var inisialBayar: UILabel!
    @IBOutlet weak var viewInisialBayar: UIView!
    @IBOutlet weak var alatBayarView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var saldo: Double = 0
    var totalTransaksi: Double = 0
    var namaPembayaran: String = ""
    var idKategori: Int = 0
    var kategoriArray = [String]()
    var idKategoriArray = [Int]()
    var judulKategoriForEachRiwayat = [Int: String]()
    var kategoriDropdown = UIPickerView()
    
    //Currency Formatter Initialization
    let formatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = NSDate()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date as Date)
        let month = calendar.component(.month, from: date as Date)
        let year = calendar.component(.year, from: date as Date)
        let hour = calendar.component(.hour, from: date as Date)
        let minute = calendar.component(.minute, from: date as Date)
        let second = calendar.component(.second, from: date as Date)
        
        var tanggalWaktuString: String = "\(day) \(month) \(year) - \(hour):\(minute):\(second)"
        
        //Date Formatter
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MM yyyy - HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy - HH:mm:ss"

        let dateFormatted: Date? = dateFormatterGet.date(from: tanggalWaktuString)
        print(dateFormatterPrint.string(from: dateFormatted!))
        
        //currency formatter
        formatter.locale = Locale(identifier: "id_ID")
        formatter.numberStyle = .currency
        
        //label for total transaksi
        if let totalTransaksiFormatted = formatter.string(from: totalTransaksi as NSNumber) {
            labelTotalTransaksi.text = String(totalTransaksiFormatted)
        }
        
        //label for total bayar
        if let totalBayarFormatted = formatter.string(from: totalTransaksi as NSNumber) {
            labelTotalBayar.text = String(totalBayarFormatted)
        }
        
        //label for saldo
        if let saldoFormatted = formatter.string(from: saldo as NSNumber) {
            labelSaldoDompet.text = String(saldoFormatted)
        }
        labelNamaPembayaran.text = namaPembayaran.uppercased()
        labelTanggalWaktu.text = dateFormatterPrint.string(from: dateFormatted!)
        inisialBayar.text = String(namaPembayaran.first!).uppercased()
        viewInisialBayar.layer.cornerRadius = viewInisialBayar.layer.frame.width / 2
        
        // Toolbar done button
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneFunction))
        
        let barAccessory = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        barAccessory.barStyle = .default
        barAccessory.isTranslucent = false
        barAccessory.items = [btnDone]
        self.kategoriTextField.inputAccessoryView = barAccessory
        
        //pickerview
        kategoriDropdown.delegate = self
        print("Kategori Array\(kategoriArray)")
        print("Id Kategori Array\(idKategoriArray)")
        kategoriTextField.inputView = kategoriDropdown
        
        var downView = UIImageView()
        var downImage = UIImage(named: "ExpandArrow")
        
        downView.image = downImage
        kategoriTextField.rightView = downView
        kategoriTextField.rightViewMode = .always
        kategoriTextField.layer.cornerRadius = 10

        alatBayarView.layer.cornerRadius = 10
        alatBayarView.layer.borderColor = CGColor(red: 229/255, green: 229/255, blue: 234/255, alpha: 1)
        alatBayarView.layer.borderWidth = 1
        
        bottomView.layer.shadowColor = UIColor.gray.cgColor
        bottomView.layer.shadowOpacity = 0.3
        bottomView.layer.shadowOffset = .zero
        bottomView.layer.shadowRadius = 5
        
        bayarButton.addTarget(self, action: #selector(confirmBayarButtonTapped), for: .touchUpInside)
        confirmBayarBackButton.addTarget(self, action: #selector(confirmBayarBackButtonTapped), for: .touchUpInside)
        
        requestAllKategori()
    }
    
    @objc func confirmBayarBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }

    
    @objc func confirmBayarButtonTapped() {
        print(idKategori)
        
        //if saldo is not enough
        if self.saldo < self.totalTransaksi {
            let alertController = UIAlertController(title: "Transaksi gagal", message: "Saldo anda tidak mencukupi", preferredStyle: .alert);
            alertController.addAction(UIAlertAction(title: "Mengerti", style: .default, handler: nil));
            self.present(alertController, animated: true, completion: nil)
        }
        //if saldo enough
        else {
            self.postPembayaran()
        }
    }
    
    func postPembayaran() {
        //request post create pembayaran
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["total": String(self.totalTransaksi), "tipe": "pengeluaran", "status": "Berhasil", "namaRiwayat": self.namaPembayaran, "iconRiwayat": "logo-bayar.png"]
        AF.request("http://localhost:8080/astratrack/riwayat/\(self.idKategori)", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    
                    let itemObject = response.value as? [String: Any]
                    let statusCode = itemObject?["status"] as? Int ?? 0
                    let data = itemObject?["data"] as? [String: Any]
                    let date = data?["date"] as! String
                    let time = data?["time"] as! String
                    let tanggalWaktu = "\(date), \(time)"
                    
                    if (statusCode == 401) {
                        UserDefaults.standard.removeObject(forKey: "loginToken")
                        self.navigationController?.popToRootViewController(animated: false)
                    }
                    else {
                        let suksesBayarViewController = self.storyboard?.instantiateViewController(withIdentifier: "SuksesBayarViewController") as! SuksesBayarViewController
                        suksesBayarViewController.namaPembayaran = self.namaPembayaran
                        
                        //label for total transaksi
                        if let totalTransaksiSuksesFormatted = self.formatter.string(from: self.totalTransaksi as NSNumber) {
                            suksesBayarViewController.jumlahTransaksi = "Transaksi sebesar -\(totalTransaksiSuksesFormatted)"
                        }
                        
                        //Date Formatter
                        let dateFormatterGet = DateFormatter()
                        dateFormatterGet.dateFormat = "yyyy-MM-dd, HH:mm:ss"

                        let dateFormatterPrint = DateFormatter()
                        dateFormatterPrint.dateFormat = "dd MMM yyyy, HH:mm:ss"

                        let suksesDateFormatted: Date? = dateFormatterGet.date(from: tanggalWaktu)
                        print(dateFormatterPrint.string(from: suksesDateFormatted!))
                    
                        suksesBayarViewController.tanggalWaktuTransaksi = "\(dateFormatterPrint.string(from: suksesDateFormatted!))"
                        
                        self.navigationController?.pushViewController(suksesBayarViewController, animated: true)
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
        
    }
    
    func requestAllKategori() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/astratrack/kategori", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    let allKategori = response.value as? [String: Any]
                    let dataKategori = allKategori?["data"] as! [[String: Any]]
                    for eachKategori in dataKategori {
                        let judulKategori = eachKategori["judulKategori"] as! String
                        let dataIdKategori = eachKategori["id"] as! Int
                        self.kategoriArray.append(judulKategori)
                        self.idKategoriArray.append(dataIdKategori)
                        if dataIdKategori == 6 {
                            self.kategoriTextField.text = "   \(judulKategori)"
                            self.idKategori = 6
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }

    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }

    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return kategoriArray.count-1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return kategoriArray[row+1]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        kategoriTextField.text = "   \(kategoriArray[row+1])"
        idKategori = idKategoriArray[row+1]
    }
    
    @objc func doneFunction() {
        //print("ini button done")
        //doneButtonTappedCallback?(idKategori)
        self.view.endEditing(true)
    }
    
}
